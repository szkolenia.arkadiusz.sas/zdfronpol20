// Import funkcji związanych z obsługą Firebase'a
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.17.1/firebase-app.js";

import {
    getDatabase,
    push,
    ref,
    onValue
} from "https://www.gstatic.com/firebasejs/9.17.1/firebase-database.js";

// Przygtowanie konfiguracji projektu Firebase'a
const firebaseConfig = {
    apiKey: "AIzaSyAYZwa5wedKN6oWfJXrvrMZDkiGeeuXzTQ",
    authDomain: "zdfronpol20-arek.firebaseapp.com",
    databaseURL: "https://zdfronpol20-arek-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "zdfronpol20-arek",
    storageBucket: "zdfronpol20-arek.appspot.com",
    messagingSenderId: "159590018331",
    appId: "1:159590018331:web:e450aa4c0f224547c977be"
};

// Uruchomienie aplikacji Firebase oraz potrzebnych nam modułów
// - uruchomienie aplikacji
const app = initializeApp(firebaseConfig);
// - uruchomienie modułu Realtime Database
const database = getDatabase(app);

// Definiowanie elementów UI
const nameInput = document.querySelector("#nameInput");
const messageInput = document.querySelector("#messageInput");
const sendMessageBtn = document.querySelector("#sendMessageBtn");
const chatArea = document.querySelector("#chatArea");

// 1. Dodanie możliwości wysyłania wiadomości
const sendMessage = () => {
    // 1.1. Pobranie imienia użytkownika z inputa
    const authorName = nameInput.value;
    // 1.2. Pobranie wiadomości z inputa
    const messageName = messageInput.value;

    // 1.3. Zapisanie imienia oraz wiadomości w bazie danych
    // push -> Dodanie do bazy z automatycznie nadanym kluczem
    // push(gdzie?, co (jaki obiekt)?)
    push(ref(database, 'messages'), {
        author: authorName,
        message: messageName
    });
};
// 1.4. Podpięcie pod przycisk funkcji wysyłającej dane do bazy
sendMessageBtn.addEventListener("click", sendMessage);

// 2. Dodanie możliwości odczytu wiadomości
// 2.1. Dodanie nasłuchiwania na zmiany pod ścieżką 'messages'
// onValue(na co ma nasłuchiwać?, co robić jak coś się zmieni?)
onValue(ref(database, 'messages'), (snapshot) => {
    // 2.2. Wygenerować widok HTML ze wszystkimi wiadomościami
    let chatContent = '';

    Object.values(snapshot.val()).forEach(message => {
        chatContent += `${message.author}: ${message.message}<br>`;
    });

    // 2.3. Umieszczenie wiadomości na stronie
    chatArea.innerHTML = chatContent;
});
